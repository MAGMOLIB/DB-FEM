function [database_metadata] = db_read_folder(filename_route)
%DB_READ_FOLDER Summary of this function goes here
%   Detailed explanation goes here


% Structure:
%   BEMF MAG1 MAG2 TORQUE.. <---- Available Simulations
%   3 1 9 9 2 4             <--- Variation_Scheme
%   'BLABLA'                <---- Label 1
%   2.0 3.0 1.0 4.0 2.0     <---- Label 1 Values
%   'BLI_BLI'               <---- Label 2
%   0.3 0.4304 .040303      <---- Label 2 Values

% Output:
%   
%   database_metadata.filename
%   database_metadata.error
%                   0 = No errors
%                   1 = File Read Error
%                   2 = Data Label Error
%                   3 = Data Label-Values Error
%                   4 = Simulation Labels File not Found
%                   5 = File not Found
%   database_metadata.Scheme
%   database_metadata.labels{1,2,3,4} CELL 1,2,3,4='STRING'
%   database_metadata.values{1,2,3,4} CELL 1,2,3,4=[FLOAT VECTORS]
%

% Error set;
database_metadata.error=0;


% Simulations Values:
fid=fopen([filename_route(1:end-4),'_sims.txt']);
% Available Simulations ()
if fid~=-1
    text_l=fgetl(fid);
    i=1;
    while ischar(text_l)
        simulations{i}=sscanf(text_l,'%s');
        text_l=fgetl(fid);
        i=i+1;
    end
else
    database_metadata.filename=fid;
    database_metadata.error=4;
    return
end
fclose(fid);

fid=fopen(filename_route);
if fid==-1
    % No need of fclose if fid its -1
    database_metadata.filename=filename_route;
    database_metadata.error=5;
    return
end

text_l=fgetl(fid);
% General file information ()
if ischar(text_l)
    scheme=sscanf(text_l,'%u',[1 Inf]);
    number_of_labels=size(scheme,2);
else
    fclose(fid);
    database_metadata.filename=filename_route;
    database_metadata.error=1;
    return
end

% Each field information ()
for i=1:1:number_of_labels
    text_l=fgetl(fid);  % Label
    if ischar(text_l)
        labels{i}=sscanf(text_l,'%c');
    else
        fclose(fid);
        database_metadata.filename=filename_route;
        database_metadata.error=2;
        return;
    end
        
    text_l=fgetl(fid);  % Values
    if ischar(text_l)
        values{i}=sscanf(text_l,'%f',[1 Inf]);
    else
        fclose(fid);
        database_metadata.filename=filename_route;
        database_metadata.error=3;
        return;
    end
end

fclose(fid);

%
if database_metadata.error==0
    for i=size(filename_route,2):-1:1
        if filename_route(i)=='\'
            filename=filename_route(i+1:end);
            route=filename_route(1:i-1);
            break
        end
    end
end


% Assigment
database_metadata.filename=filename;
database_metadata.setname=filename(1:end-4);
database_metadata.route=route;
database_metadata.simulations=simulations;
database_metadata.scheme=scheme;
database_metadata.error=0;
database_metadata.labels=labels;
database_metadata.values=values;

end

