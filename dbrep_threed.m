function [ error ] = dbrep_threed(dataset,figuren,X_label,Y_label,Magnitude_label,varargin)
% function [ no output ] = dbrep_threed(figure,X,Y,Magnitude,xlabell,ylabell,zlabell,view)
%DBREP_THREED Generates a surface with the given information.


error=0;

if nargin<5
    vieww=varargin(1);
    xlabell=varargin(2);
    ylabell=varagin(3);
    zlabell=varargin(4);
elseif (nargin<4 && nargin >1)
    error=1;
    disp('not compatible varargin set')
    return;
elseif nargin==1
    vieww=varargin(1);
end

Delta_XS=dataset.data.Delta_XS;
Delta_AngposS=dataset.data.Delta_AngposS;

if isfield(dataset.data,Magnitude_label)
    Magnitude=dataset.data.(Magnitude_label);
end

[X,Y]=meshgrid(Delta_AngposS,Delta_XS);

%   Just to make the maind code more readable.

    figure(figuren)
    surface(X,Y,Magnitude);
    xlabel(xlabell)
    ylabel(ylabell)
    zlabel(zlabell)
    grid on
    view(vieww)
    hold off


end

