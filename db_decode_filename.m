function [var_code] = db_decode_filename(filename)
%DB_DECODE_FILENAME Returns in decimal the variation of a simulation
%variation of a set given the name of the filename.

    
    % This error code implementation may not always work.
    varargout(1)=0;
    
    % Find Final Character:
    final_char=strfind(filename,'.')-5;
    if final_char==0
        var_code=0;
        %varargout(1)=1; % Error Code
        return
    end
    
    % Find Initial Character:
    initial_char=strfind(filename,'_')+1;
    
    if initial_char==0
        var_code=0;
        %varargout(1)=2;  % Error Code
        return
    end
    
    for i=1:1:final_char-initial_char+1
        var_code(i)=str2num(filename(initial_char-1+i));
    end

    
%end

