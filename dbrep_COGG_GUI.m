function [ output_args ] = dbrep_BEMF_GUI(dataset,figuren)
%DBREP_PERME_GUI Summary of this function goes here
%   Detailed explanation goes here


% Nargin Function

% ifs are required.
% size_AngposS=size(dataset.data.Delta_AngposS,2);
% Length_S=dataset.data.Length_S;
Labels=dataset.data.Labels;

dataset_in_order(:,1)=dataset.data.COGG;


Delta_AngposS=dataset.data.Delta_AngposS;



figure(figuren)
plot(Delta_AngposS,dataset_in_order(:,first_plot),'LineSmoothing','On');

xlabel([Labels{1}])
ylabel('Cogging Torque [Nm]')
legend(Labels{6},Labels{7},'Location','Best');
grid on


end

