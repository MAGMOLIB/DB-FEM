clear all
clc

addpath('C:\Users\carlescolls\Documents\MAGMOLIB\DB-FEM\FUNCTIONS\DATOS\')

%% Bad File Location for a Database:
set_info=db_read_folder('C:\Users\carlescolls\Documents\MAGMOLIB\DB-FEM\FUNCTIONS\DATOS\CL81MOD01TES2T.txt')


%pause(3)

%% Good File Location for a Database:
set_info=db_read_folder('C:\Users\carlescolls\Documents\MAGMOLIB\DB-FEM\FUNCTIONS\DATOS\CL81MOD01TEST.txt')


%pause(3)
%% User Selects certain variation in drop_down menus:

%disp('BETAM = 115 from set_info.labels{1} set_info.values{1,1} --> 1')

%disp('RADSH = 15  from set_info.labels{2} set_info.values{1,2} --> 2')

%disp('Airgap Flux Density --> Available if ''MAG1'' is present --> MAG1')

%pause(3)
%% GUI can call db_read_variation to obtain its data:

datalocation=db_recode_filename([1,2],'MAG1',set_info);

%% BACK EMF 

%datalocation=db_recode_filename([1,2],'BEMF',set_info.setname);

%% Then data is retreived:
individual_data=db_read_variation(datalocation,set_info.route,0)


%% Database Representation 3D:

dbrep_threed_GUI(individual_data,1,'BN_S')

dbrep_threed_GUI(individual_data,2,'BT_S')


%% Database Representation 2D:

dbrep_flux_GUI(individual_data,4)













