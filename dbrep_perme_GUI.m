function [ output_args ] = dbrep_perme_GUI(dataset,figuren,g,varargin)
%DBREP_PERME_GUI Summary of this function goes here
%   Detailed explanation goes here


% ifs are required.
size_AngposS=size(dataset.data.Delta_AngposS);
Length_S=dataset.data.Length_S(1);
Labels=dataset.data.Labels;
HN_S=dataset.data.HN_S;
BN_S=dataset.data.BN_S;
Delta_AngposS=dataset.data.Delta_AngposS;


%% FMM in the airgap, first approximation.
FMM=HN_S.*g;


% Flux integration:
for i=1:1:size_AngposS
    Flux(:,i)=abs(Length_S).*BN_S(:,i);
end



%% Permeance
Permeance=Flux./FMM;

for i=1:1:size_AngposS
    Permeance(i)=sum(Permeance(:,i)); % Equivalent permeance of permeances in parallel is the sum. (L2P)
end

figure(figuren)
plot(Delta_AngposS,Permeance,'LineSmoothing','On');
hold on
% plot(Delta_AngposS,Flux_TS,'-r','LineSmoothing','On');
hold off
xlabel([Labels{2}])
ylabel('Permeance')
legend(Labels{6},Labels{7},'Location','Best');

end

