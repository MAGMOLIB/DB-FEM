function [ output_args ] = dbrep_flux_GUI(dataset,figuren,varargin)
%DBREP_PERME_GUI Summary of this function goes here
%   Detailed explanation goes here

% ifs are required.
size_AngposS=size(dataset.data.Delta_AngposS,2);
Length_S=dataset.data.Length_S;
Labels=dataset.data.Labels;
BN_S=dataset.data.BN_S;
BT_S=dataset.data.BT_S;
Delta_AngposS=dataset.data.Delta_AngposS;



% Flux integration:
for i=1:1:size_AngposS
    Flux_NS(i)=sum(abs(Length_S).*BN_S(:,i));
    Flux_TS(i)=sum(abs(Length_S).*BT_S(:,i));
end


figure(figuren)
plot(Delta_AngposS,Flux_NS,'LineSmoothing','On','Linewidth',1);
hold on
plot(Delta_AngposS,Flux_TS,'-r','LineSmoothing','On','Linewidth',1);
hold off
xlabel([Labels{1}])
ylabel('Flux Integration in \tau_S [Wb]')
legend(Labels{6},Labels{7},'Location','Best');
grid on


end

