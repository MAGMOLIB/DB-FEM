function [ filename ] = db_recode_filename(var_code,simulation,set_info)
%DB_RECODE_FILENAME Summary of this function goes here
%   Detailed explanation goes here

% All elements must be bigger than zero (<-- matrix created with the sizse
% of var_code) [*]

if ~prod(single(zeros(1,size(var_code,2))<=var_code))
    disp(['Variable (',num2str(var_code),') is invalid'])
    filename='99999';
    return
end

% We check the compatibility of selected variation with available scheme:
if size(var_code)==size(set_info.scheme)
    
    if prod(single(var_code<set_info.scheme)) % See commet at the end [*]
        %% ACTUAL CORRECT FILENAME GENERATION
        for i=1:1:size(var_code,2)
            code(i)=num2str(var_code(i));
        end      
        filename=[set_info.setname,'_',code,simulation,'.xls'];
    else
        disp(['Filename ERROR: variation (',num2str(var_code),') does not belong to scheme (',num2str(set_info.scheme),')'])
        filename='99999';
        return
    end
else
    disp(['Filename ERROR: variation (',num2str(var_code),') not compatible with scheme (',num2str(set_info.scheme),')'])
    filename='99999';
    return
end

end

% [*]
% compares each element of a vector with each element of another. Creates
% a boolean vector of conditions, once multiplied (AND) you get the answer.
% problem is, prod() does not work with bool, thats why theres a single().
