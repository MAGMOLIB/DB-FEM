% Script to use in the real database
clear all
clc
addpath('C:\MAGMO_DBFEM\')

%% Bad File Location for a Database:
set_info=db_read_folder('C:\MAGMO_DBFEM\CL81MOD10.txt')


%pause(3)

%% Good File Location for a Database:
set_info=db_read_folder('C:\MAGMO_DBFEM\CL81MOD10.txt')


%pause(3)
%% User Selects certain variation in drop_down menus:

disp('BETAM = 115 from set_info.labels{1} set_info.values{1,1} --> 1')

disp('RADSH = 15  from set_info.labels{2} set_info.values{1,2} --> 2')

disp('Airgap Flux Density --> Available if ''MAG1'' is present --> MAG1')

pause(3)
%% GUI can call db_read_variation to obtain its data:

variation=[0,0,0,0,1]

datalocation=db_recode_filename(variation,'MAG1',set_info)

%% BACK EMF 

%datalocation=db_recode_filename([1,2],'BEMF',set_info.setname);

%% Then data is retreived:
debug_mode=1000;
individual_data=db_read_variation(datalocation,set_info.route,debug_mode);


%% Database Representation 3D:

dbrep_threed_GUI(individual_data,1,'BN_S')

%dbrep_threed_GUI(individual_data,2,'BT_S')


%% Database Representation 2D:

% second argument is figure numbero

%dbrep_flux_GUI(individual_data,4)

dbrep_BEMF_GUI(individual_data,4,2)













