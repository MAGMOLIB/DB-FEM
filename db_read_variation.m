function [variation_data] = db_read_variation(filename,absolute_location,debug_mode,varargin)



%% This script is the first version of computational procedure associated to db fem.

% Fake inputs:
%clear all
%clc
%filename='CL81MOD01TEST_00BEMF.xls';
%relative_location='DATOS';

%debug_mode=1;

% Because are easy to read, data will be imported from XLS files,
% tabulated.

% ISSUES: Number of columns may vary depending on exported data from
% flux. This means different data location depending on file.

% First value of X coordinate (from tau/s )


%% Program Parameters:
simutype=filename(end-7:end-4);
variation_data.error=0;
Generation='GEN01';

if debug_mode>=1 disp(['(',datestr(now),')  *** DBFEM: Individual Read ',Generation]); end

if nargin<2
    threed_representation=varargin(1);
else
    threed_representation=0;
end
if nargin<3
    time_advancement=varargin(2);
else
    time_advancement=0;
end

% Variation Reference in variation vector:

var_code=db_decode_filename(filename);
    
if ~strcmp(absolute_location,'none')        
    addpath([absolute_location]);
    if debug_mode>=1 disp(['Added ',absolute_location,'\*.* folder as DataBase LOCATION']); end
else
    disp('No relative path added')
end

%% There should be some lines here relating data imported with is corresponding geometry.

% Because of the nature of the excel file, alldata is a CELL ARRAY. 

% The following information must be extracted:
        % Values
        % Range of values
        % Meaning of the values

[pagedataS,pagetxtS]=xlsread(filename,1);

disp(['DiR - Simulation Date: ',pagetxtS{1,1}]);

%disp(['DiR - Simulation Position Ranging: ',pagetxtS{4,1}]);

%% Data positioning:
for i=1:1:100 % It is improbable that the "Values" string is on the 100th row.
    if strcmp('Values',pagetxtS(i,1))
        disp(['DiR - Units found at row ',num2str(i-1)]);
    
        % 'i' will be always greater than 2 because the 5 first rows are
        % reserved.
        Units=pagetxtS(i-1,2:end);
        Labels=pagetxtS(i-2,2:end);
        break % No more search is needed.
    end
end

[pagedatarowsS,pagedatacolumnsS]=size(pagedataS);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% DEPENDING ON SIMULATION
switch simutype
    case 'MAG1'
        datacolumnsS=pagedatacolumnsS-5;
        
        if debug_mode>=2 disp([' Number of generated variables = ',num2str(datacolumnsS)]); end
        for i=pagedatacolumnsS+1:1:pagedatacolumnsS
            disp(['Variable ',num2str(i),': ',Labels{i}]);
        end

        if debug_mode>=2 disp(' Stator Pitch ------------------------------------------'); end

        % Usually first column values are a set repeated for each second
        % column variation. This loop detects this size See [OJUT1] in
        % paper.
        
        % Worst Case Scenario:
        % There is only one value for the second column.
        size_Delta_XS=size(pagedataS,1);
        Delta_XS=pagedataS(:,1);
        
        % Support Curve Step Length:    
        for i=2:1:pagedatarowsS % Worst case scenario of single data set. [contains how many rows the data has]
            if pagedataS(i,1)==pagedataS(1,1) % <--- it is found the place where set of values is repeated again.
                pagedataS(i,1);
                size_Delta_XS=i-1;
                Delta_XS=pagedataS(1:i-1,1);
                break
            end  
        end

        Length_S(:,1)=Delta_XS(1:end-1)-Delta_XS(2:end);
        Length_S(end+1,1)=Length_S(1,1);

        %%
        if debug_mode>=2 disp(' Angular position --------------------------------------'); end
        
        % Angular Position Vector:
        size_AngposS=0;
        for i=1:size_Delta_XS:pagedatarowsS % Worst case scenario of single data set.

                size_AngposS=size_AngposS+1;
                Delta_AngposS(size_AngposS)=pagedataS(i,2);
        end

        %%
        if debug_mode>=2 disp(' FEM DATA -----------------------------------------------'); end
        if debug_mode>=2 disp(' Each column (:,i) is one angular position of the rotor, each row (j,:) is a STATOR-FIXED phyisical position in a given tau_s'); end

        %B=zeros(size_Delta_XS,size_AngposS);
        
        % Raw Data:
        for i=1:1:size_AngposS
            BN_S(:,i)=pagedataS(1+(i-1)*size_Delta_XS:(i)*size_Delta_XS,6);  % el 6 por 5+j j={1..datacolumns}
            BT_S(:,i)=pagedataS(1+(i-1)*size_Delta_XS:(i)*size_Delta_XS,7);  % el 7 por 5+j j={1..datacolumns}
            An_S(:,i)=pagedataS(1+(i-1)*size_Delta_XS:(i)*size_Delta_XS,8);  % el 8 por 5+j j={1..datacolumns}
            HN_S(:,i)=pagedataS(1+(i-1)*size_Delta_XS:(i)*size_Delta_XS,9);  % el 9 por 5+j j={1..datacolumns}
            HT_S(:,i)=pagedataS(1+(i-1)*size_Delta_XS:(i)*size_Delta_XS,10); % el 10 por 5+j j={1..datacolumns}
        end
        
          new_Labels=Labels;
%         Simulation labels
%         j=1;
%         for i=size(Labels,2)-datacolumnsS:1:size(Labels,2)
%             new_Labels{j}=Labels{i};
%             j=j+1;
%         end
    case 'BEMF'
        datacolumnsS=pagedatacolumnsS-3;
        
                disp([' Number of generated variables = ',num2str(datacolumnsS)]);
        for i=pagedatacolumnsS+1:1:pagedatacolumnsS
            disp(['Variable ',num2str(i),': ',Labels{i}]);
        end

        disp(' Angular position ---------------------------------------');
        disp(' FEM DATA -----------------------------------------------');
        disp(' Each column (:,i) is one angular position of the rotor, each row (j,:) is a STATOR-FIXED phyisical position in a given tau_s');
        
        for jj=1:1:size(pagedataS,1)
            if isnan(pagedataS(jj,1))
                disp(['-------------------------',num2str(jj),'----------------'])
                break
            end
            Delta_AngposS(jj)=pagedataS(jj,1);
            BEMF1(jj)=pagedataS(jj,2);
            BEMF2(jj)=pagedataS(jj,3);
            BEMF3(jj)=pagedataS(jj,4);
        end
        BEMF12=BEMF1-BEMF2;
        BEMF23=BEMF2-BEMF3;
        BEMF31=BEMF3-BEMF1;

        % Size Variable:
        size_AngposS=size(Delta_AngposS,1);
        
        % Simulation labelssi
        j=1;
        for i=size(Labels,2)-datacolumnsS:1:size(Labels,2)
            new_Labels{j}=Labels{i};
            j=j+1;
        end
        
    case 'COGG'
        datacolumnsS=pagedatacolumnsS-2;
        
        disp([' Number of generated variables = ',num2str(datacolumnsS)]);
        for i=pagedatacolumnsS+1:1:pagedatacolumnsS
            disp(['Variable ',num2str(i),': ',Labels{i}]);
        end
        
        disp(' Angular position ---------------------------------------');
        disp(' FEM DATA -----------------------------------------------');
        
        for jj=1:1:size(pagedataS,1)
            if isnan(pagedataS(jj,1))
                disp(['-------------------------',num2str(jj),'----------------'])
                break
            end
            Delta_AngposS(jj)=pagedataS(jj,1);
            COGG(jj)=pagedataS(jj,2);
        end
  
        % Simulation labels:
        j=1;
        for i=size(Labels,2)-datacolumnsS:1:size(Labels,2)
            new_Labels{j}=Labels{i};
            j=j+1;
        end
        
        
    case 'MAG2'
        datacolumnsS=pagedatacolumnsS-1;
    otherwise
        disp(['Error simulation data not compatible with this db_read version']);
        variation_data.error=1;
        return
end


%% [X,Y]=meshgrid(Delta_AngposS,Delta_XS);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Output Data, Common Data:
variation_data.filename=filename;
variation_data.variation=var_code;
variation_data.simutype=simutype;
variation_data.error; % Already assigned.

%% Output Data, Simulation Specific Data:
switch simutype
    case 'MAG1'
        variation_data.data.Labels=new_Labels;              % Labels of Each variation
        variation_data.data.Delta_AngposS=Delta_AngposS;    % Angular positions [DEG]    
        variation_data.data.Length_S=Length_S;              % Polar Raising in length units [m]         
        variation_data.data.Delta_XS=Delta_XS;              % Equivalent Delta_X for each Angpos [m]  
        variation_data.data.BN_S=BN_S;                      % Normal Flux Density Vector at Location [T]
        variation_data.data.BT_S=BT_S;                      % Tangencial Flux Density Vector at Location [T]
        variation_data.data.An_S=An_S;                      % Normal Vector Field at Location [A]
        variation_data.data.HN_S=HN_S;                      % Normal Field Strength at Location [A/m]
        variation_data.data.HT_S=HT_S;                      % Tangencial Field Strenght at Location [A/m]
    case 'BEMF'
        variation_data.data.Labels=new_Labels;              % Labels of Each variation
        variation_data.data.Delta_AngposS=Delta_AngposS;    % Angular position [DEG] mechanical
        variation_data.data.Delta_XS=99999;                 % Field for programming 
        variation_data.data.BEMF1=BEMF1';                   % Coil Conductor 1 Voltage. 
        variation_data.data.BEMF2=BEMF2';                   % Coil Conductor 1 Voltage. 
        variation_data.data.BEMF3=BEMF3';                   % Coil Conductor 1 Voltage. 
        variation_data.data.BEMF12=BEMF12';                 % Coil Conductor 1 Voltage. 
        variation_data.data.BEMF23=BEMF23';                 % Coil Conductor 1 Voltage. 
        variation_data.data.BEMF31=BEMF31';                 % Coil Conductor 1 Voltage.        
    case 'COGG'
        variation_data.data.Labels=new_Labels;              % Labels of Each variation
        variation_data.data.Delta_AngposS=Delta_AngposS;    % Angular position [DEG] mechanical
        variation_data.data.Delta_XS=99999;                 % Field for programming 
        variation_data.data.COGG=COGG';                     % Coil Conductor 1 Voltage. 
    otherwise
        disp(['Error simulation data not compatible with this db_read version']);
        variation_data.error=1;
        return
end


end %OF FUNCTION!!!

