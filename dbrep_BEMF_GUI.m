function [ output_args ] = dbrep_BEMF_GUI(dataset,figuren,varargin)
%DBREP_PERME_GUI Summary of this function goes here
%   Detailed explanation goes here


% Nargin Function
if nargin>2
    first_plot=cell2mat(varargin(1));
    second_plot=0;
elseif nargin>3
    first_plot=cell2mat(varargin(1));
    second_plot=cell2mat(varargin(2));
else
    first_plot=1;
    second_plot=0;
end

% ifs are required.
% size_AngposS=size(dataset.data.Delta_AngposS,2);
% Length_S=dataset.data.Length_S;
Labels=dataset.data.Labels;

dataset_in_order(:,1)=dataset.data.BEMF1;
dataset_in_order(:,2)=dataset.data.BEMF2;
dataset_in_order(:,3)=dataset.data.BEMF3;
dataset_in_order(:,4)=dataset.data.BEMF12;
dataset_in_order(:,5)=dataset.data.BEMF23;
dataset_in_order(:,6)=dataset.data.BEMF31;

Delta_AngposS=dataset.data.Delta_AngposS;



figure(figuren)
plot(Delta_AngposS,dataset_in_order(:,first_plot),'LineSmoothing','On');
if second_plot
    hold on
    plot(Delta_AngposS,dataset_in_order(:,second_plot),'-r','LineSmoothing','On');
    hold off
end
xlabel([Labels{1}])
ylabel('Flux Integration in \tau_S [Wb]')
legend(Labels{6},Labels{7},'Location','Best');
grid on


end

