function [ error ] = dbrep_threed(dataset,figuren,Magnitude_label,varargin)
% function [ no output ] = dbrep_threed(figure,X,Y,Magnitude,xlabell,ylabell,zlabell,view)
%DBREP_THREED Generates a surface with the given information.


error=0;

vieww=[45,45];
xlabell=dataset.data.Labels{1};
ylabell=dataset.data.Labels{2};

data_container_labels=fieldnames(dataset.data);

for i=1:1:length(data_container_labels)-1
    if strcmp(data_container_labels(i),Magnitude_label)
        zlabell=dataset.data.Labels{i+1};
        break
    end
end

if (nargin>1+3 && nargin<5+3)
    vieww=varargin(1);
    xlabell=varargin(2);
    ylabell=varagin(3);
    zlabell=varargin(4);
elseif (nargin<4+3 && nargin >1+3)
    error=1;
    disp('not compatible varargin set')
    return;
elseif nargin==1+3
    vieww=varargin(1);
end

if strcmp(dataset.simutype,'MAG1')
    Delta_XS=dataset.data.Delta_XS;      
    Delta_AngposS=dataset.data.Delta_AngposS;
else
    error=3;
    return
end

if isfield(dataset.data,Magnitude_label)
    Magnitude=dataset.data.(Magnitude_label);
else
    error=2;
    return
end

[X,Y]=meshgrid(Delta_AngposS,Delta_XS);

%   Just to make the maind code more readable.

    figure(figuren)
    surface(X,Y,Magnitude);
    xlabel(xlabell)
    ylabel(ylabell)
    zlabel(zlabell)
    grid on
    view(vieww)
    hold off


end

